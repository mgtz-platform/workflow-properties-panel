'use strict';

var domQuery = require('min-dom').query,
    domify = require('min-dom').domify;

var entryFieldDescription = require('./EntryFieldDescription');


var s3Key = function(translate, options, defaultParameters, eventBus) {

  // Default action for the button next to the input-field
  var selectS3Key = function(element, inputNode) {
    eventBus.fire('s3KeyField.selectRequest', {
      id: options.modelProperty,
      element,
      inputNode,
      options
    });

    return true;
  };
  // Default action for the edit button next to the input-field
  var editS3Key = function(element, inputNode) {
    eventBus.fire('s3KeyField.editRequest', {
      id: options.modelProperty,
      element,
      inputNode,
      options
    });

    return true;
  };

  var resource = defaultParameters,
      label = options.label || resource.id,
      dataValueLabel = options.dataValueLabel,
      canBeHidden = !!options.hidden && typeof options.hidden === 'function',
      description = options.description;

  var fragmentHtml =
    '<label for="camunda-' + resource.id + '" ' +
      (canBeHidden ? 'data-show="isHidden" ' : '') +
      (dataValueLabel ? 'data-value="' + dataValueLabel + '"' : '') + '>'+ label +'</label>' +
    '<div class="bpp-field-wrapper" ' +
      (canBeHidden ? 'data-show="isHidden"' : '') +
      '>' +
      '<input id="camunda-' + resource.id + '" type="text" name="' + options.modelProperty+'" ' +
        (canBeHidden ? 'data-show="isHidden"' : '') +
        ' data-disable="isDisabled"/>' +
      '<button class="bpp-input-btn" data-action="selectS3Key"' +
        (canBeHidden ? ' data-show="isHidden"' : '') + '>' +
        '<i class="fas fa-folder-open"></i>' +
      '<button class="bpp-input-btn bpp-input-btn-edit-word" data-action="editS3Key"' +
        (canBeHidden ? ' data-show="isHidden"' : '') + '>' +
        '<i class="fas fa-file-word"></i>' +
      '</button>' +
    '</div>';

    var fragment = domify(fragmentHtml);

    // add description below text input entry field
    if (description) {
      fragment.appendChild(entryFieldDescription(translate, description));
    }
  
    resource.html = fragment;

  resource.isDisabled = function() {
    return true;
  };

  resource.selectS3Key = selectS3Key;
  resource.editS3Key = editS3Key;

  if (canBeHidden) {
    resource.isHidden = function() {
      return !options.hidden.apply(resource, arguments);
    };
  }

  resource.cssClasses = ['bpp-textfield'];

  return resource;
};

module.exports = s3Key;
