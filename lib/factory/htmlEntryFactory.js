'use strict';

var entryFieldDescription = require('./EntryFieldDescription'),
    domify = require('min-dom').domify;


var html = function(translate, options, defaultParameters, eventBus) {

  // Default action for the button next to the input-field
  var openCodeEditor = function(element, inputNode) {
    eventBus.fire('htmlField.editRequest', {
      id: options.modelProperty,
      element,
      inputNode,
      options
    });

    return true;
  };

  var resource = defaultParameters,
      label = options.label || resource.id,
      canBeShown = !!options.show && typeof options.show === 'function',
      description = options.description;

  var fragmentHtml =
    '<label for="camunda-' + resource.id + '" ' +
    (canBeShown ? 'data-show="isShown"' : '') +
    '>' + label + '</label>' +
    '<div class="bpp-field-wrapper" ' +
    (canBeShown ? 'data-show="isShown"' : '') +
    '>' +
      '<div contenteditable="true" id="camunda-' + resource.id + '" ' +
            'name="' + options.modelProperty + '"></div>' +
      (eventBus ? '<button class="bpp-input-btn bpp-input-btn-edit-code" data-action="openCodeEditor" ' +
      (canBeShown ? 'data-show="isShown"' : '') +
      '>' +
        '<i class="fas fa-code"></i>' +
      '</button>' : '') +
    '</div>';

  var fragment = domify(fragmentHtml);

  // add description below text box entry field
  if (description) {
    fragment.appendChild(entryFieldDescription(translate, description));
  }
  
  resource.html = fragment;
  resource.openCodeEditor = openCodeEditor;

  if (canBeShown) {
    resource.isShown = function() {
      return options.show.apply(resource, arguments);
    };
  }

  resource.cssClasses = ['bpp-textbox'];

  return resource;
};

module.exports = html;
