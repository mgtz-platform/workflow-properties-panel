'use strict';

var domQuery = require('min-dom').query,
    domify = require('min-dom').domify;

var entryFieldDescription = require('./EntryFieldDescription');


var json = function(translate, options, defaultParameters, eventBus) {

  // Default action for the button next to the input-field
  var selectJSON = function(element, inputNode) {
    eventBus.fire('jsonField.selectRequest', {
      id: options.modelProperty,
      element,
      inputNode,
      options
    });

    return true;
  };

  var resource = defaultParameters,
      label = options.label || resource.id,
      dataValueLabel = options.dataValueLabel,
      canBeHidden = !!options.hidden && typeof options.hidden === 'function',
      description = options.description;

  var fragmentHtml =
  '<label for="camunda-' + resource.id + '" ' +
    (canBeHidden ? 'data-show="isHidden" ' : '') +
    (dataValueLabel ? 'data-value="' + dataValueLabel + '"' : '') + '>'+ label +'</label>' +
  '<div class="bpp-field-wrapper" ' +
    (canBeHidden ? 'data-show="isHidden"' : '') +
    '>' +
    '<input id="camunda-' + resource.id + '" type="text" name="' + options.modelProperty+'" ' +
      (canBeHidden ? 'data-show="isHidden"' : '') +
      ' data-disable="isDisabled"/>' +
    '<button class="bpp-input-btn" data-action="selectJSON"' +
      (canBeHidden ? ' data-show="isHidden"' : '') + '>' +
      '<i class="fas fa-code"></i>' +
    '</button>' +
  '</div>';

  var fragment = domify(fragmentHtml);

  // add description below text input entry field
  if (description) {
    fragment.appendChild(entryFieldDescription(translate, description));
  }

  resource.html = fragment;

  resource.isDisabled = function() {
    return true;
  };

  resource.selectJSON = selectJSON;

  if (canBeHidden) {
    resource.isHidden = function() {
      return !options.hidden.apply(resource, arguments);
    };
  }

  resource.cssClasses = ['bpp-textfield'];

  return resource;
};

module.exports = json;
